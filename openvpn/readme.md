OpenVPN is now starting.

Please be aware that certificate generation is variable and may take some time (minutes).

Check pod status with the command:

  POD_NAME=$(kubectl get pods --namespace "openvpn" -l app=openvpn -o jsonpath='{ .items[0].metadata.name }') && kubectl --namespace "openvpn" logs $POD_NAME --follow

LoadBalancer ingress creation can take some time as well. Check service status with the command:

  kubectl --namespace "openvpn" get svc

Once the external IP is available and all the server certificates are generated create client key .ovpn files by pasting the following into a shell:

  POD_NAME=$(kubectl get pods --namespace "openvpn" -l "app=openvpn,release=openvpn" -o jsonpath='{ .items[0].metadata.name }')
  SERVICE_NAME=$(kubectl get svc --namespace "openvpn" -l "app=openvpn,release=openvpn" -o jsonpath='{ .items[0].metadata.name }')
  SERVICE_IP=$(kubectl get svc --namespace "openvpn" "$SERVICE_NAME" -o go-template='{{ range $k, $v := (index .status.loadBalancer.ingress 0)}}{{ $v }}{{end}}')
  KEY_NAME=kubeVPN
  kubectl --namespace "openvpn" exec -it "$POD_NAME" /etc/openvpn/setup/newClientCert.sh "$KEY_NAME" "$SERVICE_IP"
  kubectl --namespace "openvpn" exec -it "$POD_NAME" cat "/etc/openvpn/certs/pki/$KEY_NAME.ovpn" > "$KEY_NAME.ovpn"

Revoking certificates works just as easy:
  KEY_NAME=<name>
  POD_NAME=$(kubectl get pods -n "openvpn" -l "app=openvpn,release=openvpn" -o jsonpath='{.items[0].metadata.name}')
  kubectl -n "openvpn" exec -it "$POD_NAME" /etc/openvpn/setup/revokeClientCert.sh $KEY_NAME

Copy the resulting $KEY_NAME.ovpn file to your open vpn client (ex: in tunnelblick, just double click on the file).  Do this for each user that needs to connect to the VPN.  Change KEY_NAME for each additional user.